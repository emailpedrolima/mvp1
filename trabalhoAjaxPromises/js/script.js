
let nome =  document.getElementById("nome");
let msg = document.getElementById("msg");
let enviar = document.getElementById("enviar");
let listar = document.getElementById("obterMSG")

 function adicionarMensagem(){


     const fetchBody = {
        "message":{
           "name": nome.value,
           "message": msg.value
    }
        }   
      
      const fetchConfig = {
          method: "POST",
          headers: {"Content-Type":"application/JSON"},
          body: JSON.stringify(fetchBody)    
          }
      

      
    fetch("https://treinamentoajax.herokuapp.com/messages/", fetchConfig)
    nome.value = "";
    msg.value = "";
}


function listarMensagem(){
    Mensagens.innerHTML = ""
    fetch("https://treinamentoajax.herokuapp.com/messages")
        .then(response => response.json())
        .then(response => {
            for (i in response){
                let card = document.createElement("article");
                let h1 = document.createElement("h1");
                let p =  document.createElement("p");
                h1.innerHTML = response[i].name;
                p.innerHTML = response[i].message;
                card.appendChild(h1);
                card.appendChild(p);
                card.id = response[i].id;
                Mensagens.appendChild(card);
    
    

}})
}



enviar.addEventListener('click', adicionarMensagem);
listar.addEventListener('click', listarMensagem);
